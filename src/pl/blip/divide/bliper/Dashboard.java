/*
    Bliper - a blip.pl client for Android
    Copyright (C) 2009  Rafał Rzepecki

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package pl.blip.divide.bliper;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.blip.divide.bliper.DashboardView.DashboardAdapter;
import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;

public class Dashboard extends Activity {
	public class SendListener implements OnClickListener, OnEditorActionListener {
		public void onClick(View v) {
			doSend();
		}

		private void doSend() {
			if (newStatus == null)
				return;
			newStatus = insertingStatus = myStatusView.getText().toString();
			
			final ContentValues cv = new ContentValues();
			cv.put("update[body]", newStatus);
			myselfHandler.startInsert(0, null, Blip.Updates.CONTENT_URI, cv);
		}

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (event == null || event.getAction() != KeyEvent.ACTION_UP)
				return true;
			doSend();
			return true;
		}
	}

	private ImageView myAvatarView;
	
	private String currentStatus = null;
	private String newStatus = null;
	private String insertingStatus = null;

	private class MyselfHandler extends AsyncQueryHandler {
		private final String[] PROJECTION = { "current_status/body", "avatar/url_50" };
		private final int BODY_COLUMN = 0, AVATAR_COLUMN = 1;
		private Uri uri;

		public MyselfHandler(String login) {
			super(getContentResolver());
			this.uri = Uri.withAppendedPath(Blip.Users.CONTENT_URI, login);
		}
		
		private void startQuery() {
			startQuery(0, null, uri, PROJECTION, null, null, null);
		}
		
		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
			super.onQueryComplete(token, cookie, cursor);
		
			if (cursor == null || !cursor.moveToFirst()) {
				if (!initialized) {
					Toast.makeText(Dashboard.this, R.string.connection_unsuccessful, Toast.LENGTH_LONG).show();
					finish();
				}
				return;
			}
			
			if (!initialized)
				finishInitializing();

			currentStatus = cursor.getString(BODY_COLUMN);

			if (!myStatusView.hasFocus() && newStatus == null)
				myStatusView.setText(currentStatus);

			if (myAvatarView != null)
				myAvatarView.setImageURI(Uri.withAppendedPath(Blip.CONTENT_URI, cursor.getString(AVATAR_COLUMN)));
		}
		
		@Override
		protected void onInsertComplete(int token, Object cookie, Uri uri) {
			super.onInsertComplete(token, cookie, uri);
			
			if (uri != null) {
				if (!insertingStatus.matches("^>>?.*:.*"))
					currentStatus = insertingStatus;
				newStatus = null;
				updateMyStatus(true);
				updateMyStatus(myStatusView.hasFocus());
				
				if (updatesView != null)
					updatesView.updateNow();
			}
		}
	}
	
	private String login;
	private boolean initialized = false;
	private EditText myStatusView;
	private MyselfHandler myselfHandler;
	private ScheduledExecutorService executor;
	private DashboardView updatesView;
	private OnFocusChangeListener myStatusViewFocusWatcher = new OnFocusChangeListener() {
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			updateMyStatus(hasFocus);
		}
	};

	private Button sendButton;

	private SendListener sendListener = new SendListener();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		login = Blip.getUsername(this);
	
		if (login == null) {
			Intent i = new Intent(this, LoginActivity.class);
			startActivity(i);
			finish();
			return;
		}
		
		myselfHandler = new MyselfHandler(login);
		myselfHandler.startQuery();

		setContentView(R.layout.dashboard);
		myStatusView = (EditText) findViewById(R.id.status);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// so it'll get correctly frozen by android
		myStatusView.setText(newStatus == null ? "" : newStatus);
		super.onSaveInstanceState(outState);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		// in case we were just thawed by Android, retrieve the status
		final String status = myStatusView.getText().toString();
		if (status.length() > 0)
			newStatus = status;
	}

	public void updateMyStatus(boolean hasFocus) {
		if (hasFocus) {
			if (newStatus == null) {
				newStatus = "";
				myStatusView.setText(newStatus);
			}
			myStatusView.setBackgroundResource(android.R.drawable.edit_text);
			myStatusView.setTextColor(getResources().getColorStateList(android.R.color.primary_text_light));
		} else {
			newStatus = myStatusView.getText().toString();
			if (newStatus == null || newStatus.length() == 0) {
				newStatus = null;
				myStatusView.setText(currentStatus);
				myStatusView.setBackgroundResource(R.drawable.textfield_default_dormant);
				myStatusView.setTextColor(getResources().getColorStateList(R.drawable.custom_status_textfield_color));
			}
		}
		
		// TODO make it extract values from XML or whatever
		myStatusView.setPadding(5, 2, 37, 5);
	}

	public void finishInitializing() {
		setTitle(R.string.dashboard);
		
		myAvatarView = (ImageView) findViewById(R.id.avatar);
		updatesView = (DashboardView) findViewById(R.id.updates);
		sendButton = (Button) findViewById(R.id.send);
		
		myStatusView.setOnFocusChangeListener(myStatusViewFocusWatcher);
		myStatusView.setOnEditorActionListener(sendListener);
		
		updatesView.bindDashboard();
		updatesView.startUpdating();
		updatesView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> view, View itemView, int position, long id) {
				tryToQuote(position);
			}
		});
		
		sendButton.setOnClickListener(sendListener);
		sendButton.requestFocus();
		
		initialized = true;
	}
	
	protected void tryToQuote(int position) {
		Cursor c = (Cursor) updatesView.getAdapter().getItem(position);
		final String type = c.getString(DashboardAdapter.TYPE_COLUMN);
		String user = c.getString(DashboardAdapter.AUTHOR_COLUMN);
		if (user.equals(login)) {
			final String recipient = c.getString(DashboardAdapter.RECIPIENT_COLUMN);
			if (recipient != null)
				user = recipient;
			else
				return;
		}
		
		final StringBuilder messageBuilder = new StringBuilder();
		if (type.equals(Blip.PRIVATE_MESSAGE_TYPE))
			messageBuilder.append(">>" + user + ": ");
		else
			messageBuilder.append(">" + user + ": ");
		
		if (newStatus != null) {
			newStatus = myStatusView.getText().toString();
			Matcher m = Pattern.compile("^>>?.*?: *(.*)$").matcher(newStatus);
			if (m.matches())
				messageBuilder.append(m.group(1));
		}
		
		myStatusView.requestFocus();
		newStatus = messageBuilder.toString();
		myStatusView.setText(newStatus);
		myStatusView.setSelection(newStatus.length());
	}

	private void scheduleQuerying() {
		executor = Executors.newSingleThreadScheduledExecutor();
		Runnable requeryMyselfCommand = new Runnable() {
			@Override
			public void run() {
				myselfHandler.startQuery();
			}
		};
		executor.scheduleAtFixedRate(requeryMyselfCommand, 20, 20, TimeUnit.SECONDS);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MessagePoller.startPolling(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		MessagePoller.stopPolling(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		executor.shutdownNow();
		if (updatesView != null)
			updatesView.stopUpdating();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		myselfHandler.startQuery();
		scheduleQuerying();
		
		if (updatesView != null)
			updatesView.startUpdating();
	}
	
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		if (updatesView != null)
			updatesView.onUserInteraction();
	}
}
