/*
    Bliper - a blip.pl client for Android
    Copyright (C) 2009  Rafał Rzepecki

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package pl.blip.divide.bliper;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.Context;
import android.database.Cursor;
import android.database.MergeCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class DashboardView extends ListView {
	private Handler handler = new Handler();
	private static class UpdateTextBuilder extends SpannableStringBuilder {
		public void appendBold(String string) {
			final int position = length();
			append(string);
			setSpan(new StyleSpan(Typeface.BOLD), position, length(), 0);
		}
	}

	public class DashboardAdapter extends CursorAdapter {
		private LayoutInflater inflater;
		private final Uri uri = Blip.DASHBOARD_URI;
		private LinkedList<Cursor> cursors = new LinkedList<Cursor>();
		private Integer last_id = null;
		private boolean queryRunning = false;
		
		private class QueryHandler extends AsyncQueryHandler {
			public QueryHandler() {
				super(getContext().getContentResolver());
			}

			public void startQuery() {
				if (queryRunning)
					return;
				else
					queryRunning = true;
				Uri queryUri = last_id == null ? uri : Uri.withAppendedPath(uri, "since/" + last_id.toString());
				startQuery((last_id == null ? 0 : last_id), null, queryUri, PROJECTION, null, null, null);
			}
			
			@Override
			protected void onQueryComplete(int token, Object cookie,
					Cursor cursor) {
				super.onQueryComplete(token, cookie, cursor);
				if ((last_id == null || token == last_id) && cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
					last_id = cursor.getInt(ID_COLUMN);
					cursors.addFirst(cursor);
					Cursor cursorArray[] = cursors.toArray(new Cursor[0]);
					changeCursor(new MergeCursor(cursorArray));
				}
				queryRunning = false;
			}
		}
		
		private final QueryHandler queryHandler = new QueryHandler();
		
		private final String[] PROJECTION = {
				"_id", "body", "user/avatar/url_50", "type", "user/login", "recipient/login", "recipient/avatar/url_50"
		};
		public static final int ID_COLUMN = 0, BODY_COLUMN = 1, AVATAR_COLUMN = 2, TYPE_COLUMN = 3, AUTHOR_COLUMN = 4, 
			RECIPIENT_COLUMN = 5, RECIPIENT_AVATAR_COLUMN = 6;
		private AvatarLoader avatarLoader;
		private ScheduledExecutorService executor;
		
		private class AvatarLoader {
			private ExecutorService executor = Executors.newSingleThreadExecutor();
			private Map<View, Future<?>> fetches = new HashMap<View, Future<?>>();
			private Map<Uri, Bitmap> bitmaps = new HashMap<Uri, Bitmap>();
			private Handler handler = new Handler();

			public void load(final ImageView view, final Context context, final Uri uri) {
				Future<?> oldFetch = fetches.get(view);
				if (bitmaps.containsKey(uri))
					view.setImageBitmap(bitmaps.get(uri));
				else
					fetches.put(view, executor.submit(new Runnable() {
						@Override
						public void run() {
							try {
								final Bitmap b = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri));
								handler.post(new Runnable() {
									public void run() {
										bitmaps.put(uri, b);
										view.setImageBitmap(b);
									}
								});
							} catch (FileNotFoundException e) {
								Log.e("bliper/DashboardView", "error while fetching avatar from " + uri.toString(), e);
							}
						}
					}));
				
				if (oldFetch != null)
					oldFetch.cancel(true);
			}

			public void cancelLoading(ImageView view) {
				Future<?> oldFetch = fetches.put(view, null);
				
				if (oldFetch != null) {
					oldFetch.cancel(true);
				}
			}
		}

		public DashboardAdapter(Context context) {
			super(context, null, false);
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			avatarLoader = new AvatarLoader();
		}
		
		private void scheduleRequerying() {
			executor = Executors.newSingleThreadScheduledExecutor();
			executor.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					queryHandler.startQuery();
				}
			}, 0, 20, TimeUnit.SECONDS);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TextView bodyView = (TextView) view.findViewById(R.id.update);
			ImageView avatarView = (ImageView) view.findViewById(R.id.avatar);
			ImageView rcptAvatarView = (ImageView) view.findViewById(R.id.rcpt_avatar);
			ImageView decorationView = (ImageView) view.findViewById(R.id.decoration);

			UpdateTextBuilder updateText = new UpdateTextBuilder();
			
			final String type = cursor.getString(TYPE_COLUMN); 
			if (type.equals("Notice")) {
				decorationView.setVisibility(View.VISIBLE);
				decorationView.setImageResource(android.R.drawable.ic_dialog_info);
				avatarView.setVisibility(View.GONE);
				if (rcptAvatarView != null)
					rcptAvatarView.setVisibility(View.GONE);
			} else {
				decorationView.setVisibility(View.GONE);
				avatarView.setVisibility(View.VISIBLE);

				updateText.appendBold(cursor.getString(AUTHOR_COLUMN));

				if (avatarView != null) {
					avatarView.setImageResource(R.drawable.nn_standard);
					if (!cursor.isNull(AVATAR_COLUMN))
						avatarLoader.load(avatarView, context, Uri.withAppendedPath(Blip.CONTENT_URI, cursor.getString(AVATAR_COLUMN)));
				}

				if (type.equals("Status")) {
					if (rcptAvatarView != null)
						rcptAvatarView.setVisibility(View.GONE);
				} else {
					if (rcptAvatarView != null) {
						rcptAvatarView.setVisibility(View.VISIBLE);
						rcptAvatarView.setImageResource(R.drawable.nn_standard);
						if (!cursor.isNull(RECIPIENT_AVATAR_COLUMN))
							avatarLoader.load(rcptAvatarView, context, 
									Uri.withAppendedPath(Blip.CONTENT_URI, cursor.getString(RECIPIENT_AVATAR_COLUMN)));
						else
							avatarLoader.cancelLoading(rcptAvatarView);
					}
					
					if (type.equals(Blip.PRIVATE_MESSAGE_TYPE))
						updateText.append(" >> ");
					else
						updateText.append(" > ");
					
					updateText.appendBold(cursor.getString(RECIPIENT_COLUMN));
				}
				
				updateText.append(": ");
			}
			
			updateText.append(cursor.getString(BODY_COLUMN));
				
			bodyView.setText(updateText);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			return inflater.inflate(R.layout.update_list_item, parent, false); 
		}
		
		@Override
		public void changeCursor(Cursor cursor) {
			boolean followTop = getScrollY() == 0;
			super.changeCursor(cursor);
			if (followTop)
				handler.post(new Runnable() {
					@Override
					public void run() {
						setSelection(0);
					}
				});
		}

		public void stopUpdating() {
			executor.shutdownNow();
		}

		public void updateNow() {
			queryHandler.startQuery();
		}

		public void resetPolling() {
			if (last_id != null)
				MessagePoller.resetPolling(getContext(), last_id);
		}
	}

	private Activity activity;
	private DashboardAdapter adapter;

	public DashboardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		activity = (Activity) context;
	}

	public void bindDashboard() {
		setAdapter(adapter = new DashboardAdapter(activity));
	}

	public void stopUpdating() {
		if (adapter != null)
			adapter.stopUpdating();
	}

	public void startUpdating() {
		if (adapter != null)
			adapter.scheduleRequerying();
	}

	public void updateNow() {
		if (adapter != null)
			adapter.updateNow();
	}

	public void onUserInteraction() {
		if (adapter != null)
			adapter.resetPolling();
	}
}
