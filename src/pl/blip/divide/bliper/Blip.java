/*
    Bliper - a blip.pl client for Android
    Copyright (C) 2009  Rafał Rzepecki

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package pl.blip.divide.bliper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.ContentHandler;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;

public class Blip extends ContentProvider {
	static public String DIRECTED_MESSAGE_TYPE = "DirectedMessage";
	static public String PRIVATE_MESSAGE_TYPE = "PrivateMessage";
	
	private class BlipCursor extends MatrixCursor {
		private Uri uri;
		private String[] projection;

		public BlipCursor(Uri uri, String[] projection) throws BadCredentials, IOException {
			super(projection);
			this.uri = uri;
			this.projection = projection;
			
			query();
		}
		
		private void query() throws BadCredentials, IOException {
			Object result;
			result = get(uri.getPath());
			
			if (result == null)
				throw new FileNotFoundException(uri.toString());
			
			if (result instanceof JSONObject)
				addRow(projectJSON((JSONObject) result, projection));
			else {
				final JSONArray arr = (JSONArray) result;
				final int size = arr.length();
				for (int i = 0; i < size; i++)
					try {
						addRow(projectJSON(arr.getJSONObject(i), projection));
					} catch (JSONException e) {
						log_error("error while building result for querying " + uri.toString(), e);
						throw new FileNotFoundException(e.getMessage());
					}
			}
		}
	}

	private static class BlipURLStreamFactory implements URLStreamFactory {
		private String username, password;
		
		public BlipURLStreamFactory(String username, String password) {
			this.username = username;
			this.password = password;
		}

		@Override
		public InputStream openStream(URL url) throws IOException {
			Log.d("BlipURLStreamFactory", "Opening " + url);
			final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.addRequestProperty("X-Blip-Api", "0.02");
			connection.addRequestProperty("Accept", "application/json");
			connection.addRequestProperty("User-Agent", "Bliper/0.1");
			if (username != null) {
				connection.addRequestProperty("Authorization", "Basic " + Base64Coder.encodeString(username + ":" + password));
			}
			connection.connect();
			if (connection.getResponseCode() == 401)
				throw new BadCredentials();
			return connection.getInputStream();
		}

		public void post(URL url, String output) throws IOException {
			Log.d("BlipURLStreamFactory", "POSTing to " + url);
			final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.addRequestProperty("X-Blip-Api", "0.02");
			connection.addRequestProperty("Accept", "application/json");
			connection.addRequestProperty("User-Agent", "Bliper/0.1");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			if (username != null) {
				connection.addRequestProperty("Authorization", "Basic " + Base64Coder.encodeString(username + ":" + password));
			}
			connection.connect();
			
			OutputStream os = connection.getOutputStream();
			DataOutputStream dos = new DataOutputStream(os);
			dos.writeBytes(output);
			dos.flush();
			dos.close();

			int response = connection.getResponseCode();
			String message = connection.getResponseMessage();
			connection.disconnect();
			Log.d("BlipURLStreamFactory", "response code is " + response);
			
			if (response >= 400)
				throw new HttpResponseException(response, message);
		}
	}

	private static class StandardURLStreamFactory implements URLStreamFactory {
		@Override
		public InputStream openStream(URL url) throws IOException {
			System.err.println("Opening " + url + " using StandardURLStreamFactory");
			return url.openStream();
		}
	}

	private interface URLStreamFactory {
		InputStream openStream(URL url) throws IOException;
	}

	public class BlipContentHandler extends ContentHandler {
		@Override
		public Object getContent(URLConnection conn) throws IOException {
			return JSONFromStream(conn.getInputStream());
		}
	}

	public static Object JSONFromStream(final InputStream inputStream) throws IOException {
		final InputStreamReader isr = new InputStreamReader(inputStream, Charset.forName("utf-8"));
		final BufferedReader br = new BufferedReader(isr, 1024 /* should be enough */);
		final StringWriter sw = new StringWriter();
		String line;
		while ((line = br.readLine()) != null)
			sw.write(line);
		
		try {
			final String content = sw.toString();
			if (content.charAt(0) == '{')
				return new JSONObject(content);
			else
				return new JSONArray(content);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (StringIndexOutOfBoundsException e) {
			// empty object
			Log.d("Blip", "got empty object");
			return new JSONArray();
		}
	}

	public static void log_error(String msg, Throwable e) {
		Log.e("Blip", msg, e);
	}

	public static class BadCredentials extends IOException {
		private static final long serialVersionUID = -5503599181549720716L;
	}
	
	public static class Users {
		public static final Uri CONTENT_URI = Uri.parse("content://blip.pl/users");
	}
	public static class Updates {
		public static final Uri CONTENT_URI = Uri.parse("content://blip.pl/updates");
	}

	public static final Uri CONTENT_URI = Uri.parse("content://blip.pl");
	public static final Uri DASHBOARD_URI = Uri.parse("content://blip.pl/dashboard");

	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private String login;
	private String password;
	private static URLStreamFactory standardStreamFactory = new StandardURLStreamFactory();
	private BlipURLStreamFactory blipStreamFactory;

	private File cacheDir;
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		try {
			final String output = formEncode(values);
			blipStreamFactory.post(new URL("http", "api.blip.pl", -1, uri.getPath()), output);
			return uri;
		} catch (IOException e) {
			Log.e("Blip", "error while inserting into " + uri.toString(), e);
			return null;
		}
	}

	private static String formEncode(ContentValues values) {
		final Set<Entry<String, Object>>valueSet = values.valueSet();
		List<NameValuePair> parameters = new LinkedList<NameValuePair>();
		
		for (final Entry<String, Object> e: valueSet) {
			parameters.add(new NameValuePair() {
				@Override
				public String getName() {
					return e.getKey();
				}

				@Override
				public String getValue() {
					return e.getValue().toString();
				}
			});
		}
		
		return URLEncodedUtils.format(parameters, "UTF-8");
	}

	@Override
	public boolean onCreate() {
		cacheDir = getContext().getCacheDir();
		loadCredentials();
		blipStreamFactory = new BlipURLStreamFactory(login, password);
		return true;
	}

	private void loadCredentials() {
		SharedPreferences prefs = getSharedPreferences(getContext());
		login = prefs.getString(USERNAME, null);
		password = prefs.getString(PASSWORD, null);
		blipStreamFactory = new BlipURLStreamFactory(login, password);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		if (password == null)
			loadCredentials();
		
		try {
			return new BlipCursor(uri, projection);
		} catch (FileNotFoundException e) {
			Log.d("Blip", "file not found when querying " + uri.toString());
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log_error("Error while querying", e);
			return null;
		}
	}

	private String[] projectJSON(JSONObject object, String[] projection) {
		String[] result = new String[projection.length];
		
		for (int i = 0; i < projection.length; i++) {
			List<String> path;
			if (projection[i].equals("_id"))
				path = Arrays.asList(new String[] {"id"});
			else
				path = Arrays.asList(projection[i].split("/"));
			try {
				result[i] = extractJSON(object, path);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}

	private String extractJSON(JSONObject object, List<String> path) throws JSONException, IOException, BadCredentials {
		if (object == null)
			return null;
		
		if (path.size() == 1)
			return object.getString(path.get(0));
		
		final String root = path.get(0);
		
		JSONObject child = null;
		if (object.has(root))
			child = object.getJSONObject(root);
		else if (object.has(root + "_path")) {
			String childPath = object.getString(root + "_path");
			// special cases for network request count optimization
			final Matcher m = Pattern.compile("/users/(.*)$").matcher(childPath);
			if (m.matches()) {
				final String next = path.get(1);
				if (next.equals("login"))
					// extract login from path
					return m.group(1);
				else if (next.equals("avatar")) {
					// go straight to avatar
					Object theChild = get("/users/" + m.group(1) + "/avatar");
					if (theChild instanceof JSONObject) child = (JSONObject) theChild;
					else return null; // no avatar
					return extractJSON(child, path.subList(2, path.size()));
				}
			}
			// end of special cases
			Object theChild = get(childPath);
			if (theChild instanceof JSONObject) child = (JSONObject) theChild;
			else return null; // not expected, better that than TODO
		}
		
		return extractJSON(child, path.subList(1, path.size()));
	}

	private Object get(String path) throws IOException, BadCredentials {
		return JSONFromStream(new FileInputStream(cachedResource(new URL("http", "api.blip.pl", -1, path), blipStreamFactory)));
	}
	
	@Override
	public ParcelFileDescriptor openFile(Uri uri, String mode)
			throws FileNotFoundException {
		URL url;
		try {
			url = new URL("http", "blip.pl", -1, uri.getPath());
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new FileNotFoundException(uri.toString());
		}
		
		return ParcelFileDescriptor.open(cachedResource(url, standardStreamFactory), ParcelFileDescriptor.MODE_READ_ONLY);
	}

	private File cachedResource(URL url, URLStreamFactory factory) throws FileNotFoundException {
		final File cachedFile = new File(cacheDir + File.separator + url.getPath() + "/.data"); // the suffix is to avoid file/dir clashes
		
		final boolean exists = cachedFile.exists();
		final boolean isStale = !exists || cacheIsStale(cachedFile);
		if (!exists || isStale) try {
			cachedFile.getParentFile().mkdirs();
			
			final FileOutputStream fos = new FileOutputStream(cachedFile);
			final BufferedInputStream bis = new BufferedInputStream(factory.openStream(url), 1024);
			final int BUFSIZE = 1024;
			byte buffer[] = new byte[BUFSIZE];
			int count;
			while ((count = bis.read(buffer)) != -1) {
				fos.write(buffer, 0, count);
			}
			fos.close();
		} catch (IOException e) {
			System.err.println("Error while getting " + url.toString());
			e.printStackTrace();
			if (!exists) throw new FileNotFoundException(e.getMessage());
		}
		
		return cachedFile;
	}

	private boolean cacheIsStale(File file) {
		long max_age = 0;
		final String path = file.getAbsolutePath();
		if (path.matches(".*/users/.*?/avatar/.*"))
			max_age = 60 * 60 * 1000; // an hour
		else if (path.matches(".*/users/.*?/.*"))
			max_age = 0; // refreshed as often as it's requested
		else if (path.matches(".*/dashboard/.*"))
			max_age = 0; // refreshed as often as it's requested
		else if (path.matches(".*/since/.*"))
			max_age = 0; // refreshed as often as it's requested
		else
			return false; // assume everything else is immutable

		final long age = System.currentTimeMillis() - file.lastModified();
		return age > max_age;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static String getUsername(Context context) {
		final SharedPreferences prefs = getSharedPreferences(context);
		return prefs.getString(USERNAME, null);
	}

	private static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences("blip.pl.credentials", Context.MODE_PRIVATE);
	}

	public static void setCredentials(Context context,
			String username, String password) throws BadCredentials, IOException {
		if (!checkCredentials(username, password))
			throw new BadCredentials();
		
		final SharedPreferences prefs = getSharedPreferences(context);
		final Editor editor = prefs.edit();
		editor.putString(USERNAME, username).putString(PASSWORD, password).commit();
	}

	private static boolean checkCredentials(String username, String password) throws IOException {
		try {
			URLStreamFactory factory = new BlipURLStreamFactory(username, password);
			factory.openStream(new URL("http://api.blip.pl/updates?limit=1"));
		} catch (BadCredentials e) {
			return false;
		}
		return true;
	}
}
