/*
    Bliper - a blip.pl client for Android
    Copyright (C) 2009  Rafał Rzepecki

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package pl.blip.divide.bliper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import pl.blip.divide.bliper.Blip.BadCredentials;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener, OnCancelListener, TextWatcher {
	private Button loginButton;
	private EditText usernameEdit;
	private EditText passwordEdit;
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	private Future<?> loggingIn;
	private ProgressDialog progressDialog = null;
	private Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		
		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(R.layout.login_activity, null);
		setContentView(view);
		
		loginButton = (Button) view.findViewById(R.id.login);
		usernameEdit = (EditText) view.findViewById(R.id.username);
		passwordEdit = (EditText) view.findViewById(R.id.password);
		
		usernameEdit.addTextChangedListener(this);
		passwordEdit.addTextChangedListener(this);
		loginButton.setOnClickListener(this);
		loginButton.setEnabled(false);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if (progressDialog != null)
			progressDialog.dismiss();
	}

	@Override
	public void onClick(View v) {
		if (v == loginButton)
			doLogin();
	}

	private void doLogin() {
		final String username = usernameEdit.getText().toString();
		final String password = passwordEdit.getText().toString();
		
		progressDialog = ProgressDialog.show(this, getString(R.string.app_name), getString(R.string.logging_in), true, true, this);
		loggingIn = makeLoginProcess(username, password);
	}

	private Future<?> makeLoginProcess(final String username, final String password) {
		return executor.submit(new Runnable() {
			@Override
			public void run() {
				String errorMessage = "";
				try {
					Blip.setCredentials(LoginActivity.this, username, password);
					reportLoginSuccess();
					return;
				} catch (BadCredentials e) {
					errorMessage = getString(R.string.invalid_credentials);
				} catch (Exception e) {
					// TODO do something smarter
					errorMessage = getString(R.string.error) + e.getMessage();
					e.printStackTrace();
				}
				reportLoginFailure(errorMessage);
			}
		});
	}
	
	protected void reportLoginSuccess() {
		dismissDialog();
		handler.post(new Runnable() {
			@Override
			public void run() {
				loginSuccess();
			}
		});
	}

	private void dismissDialog() {
		try {
			progressDialog.dismiss();
		} catch (IllegalArgumentException e) {
			// sometimes gets thrown when rotating phone in midflight, well let's just ignore it
		}
	}

	protected void loginSuccess() {
		finish();

		Intent i = new Intent(LoginActivity.this, Dashboard.class);
		startActivity(i);
		
		Toast.makeText(LoginActivity.this, R.string.logged_in_success, Toast.LENGTH_SHORT).show();
	}

	protected void reportLoginFailure(final String message) {
		dismissDialog();
		handler.post(new Runnable() {
			@Override
			public void run() {
				final Toast t = Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG);
				t.show();
				usernameEdit.requestFocus();
			}
		});
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		loggingIn.cancel(true);
	}

	@Override
	public void afterTextChanged(Editable s) {
		loginButton.setEnabled(!(usernameEdit.getText().length() == 0 || passwordEdit.getText().length() == 0));
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// couldn't care less
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// couldn't care less
	}
}
