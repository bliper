/*
    Bliper - a blip.pl client for Android
    Copyright (C) 2009  Rafał Rzepecki

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package pl.blip.divide.bliper;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

public class MessagePoller extends BroadcastReceiver {

	public class MessagePoll implements Callable<Cursor> {
		private ContentResolver contentResolver;
		private int last;

		public MessagePoll(ContentResolver contentResolver, int last) {
			this.contentResolver = contentResolver;
			this.last = last;
		}

		@Override
		public Cursor call() {
			final Cursor dmc = contentResolver.query(
					ContentUris.withAppendedId(Uri.withAppendedPath(Blip.CONTENT_URI, "directed_messages/since"), last),  
					PROJECTION, null, null, null);
			final Cursor pmc = contentResolver.query(
					ContentUris.withAppendedId(Uri.withAppendedPath(Blip.CONTENT_URI, "private_messages/since"), last),  
					PROJECTION, null, null, null);
			return new MergeCursor(new Cursor[] { dmc, pmc });
		}
	}

	private static final String[] PROJECTION = { "user/login", "body", "_id" };
	private static final int SENDER_COLUMN = 0;
	private static final int BODY_COLUMN = 1;
	private static final int ID_COLUMN = 2;
	private static final int NOTIFICATION_ID = 0;
	private static final String FREQUENCY_KEY = "frequency";
	private static final int INITIAL_FREQUENCY_MS = 1000;
	private static final int FREQUENCY_DELTA_MS = 1000;
	private static final int MAX_FREQUENCY_MS = 1000 * 60 * 5; // don't poll less often than every five minutes
	private static final String MESSAGE_COUNT_KEY = "message_count";
	private static final String LAST_SEEN_KEY = "last_seen";
	private static boolean IS_DREAM = Build.DEVICE.equals("dream");
	private SharedPreferences sharedPreferences;

	@Override
	public void onReceive(Context context, Intent intent) {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		sharedPreferences = getSharedPreferences(context);
		final int last = getLastSeen();
		Future<Cursor> future = executor.submit(new MessagePoll(context.getContentResolver(), last));

		Cursor c = null;
		try {
			// 9 seconds so that we still have 1 second for processing before ANR kicks in
			c = future.get(9, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			// most possibly network failure, abort, 
			// perhaps we'll have better luck next time
			future.cancel(true);
		} catch (Exception e) {
			// something went wrong, don't alarm the user
			// but print stack trace anyway, just in case
			Log.e(getClass().getName(), "Error while fetching messages.", e);
		}
		
		if (c != null && c.moveToFirst()) {
			int countDelta = 0;
			final String myLogin = Blip.getUsername(context);
			
			int firstOther = -1;

			do {
				if (c.getString(SENDER_COLUMN).equals(myLogin))
					countDelta++;
				else
					if (firstOther == -1)
						firstOther = c.getPosition();
			} while (c.moveToNext());
			
			Notification n = null;
			if (firstOther != -1) {
				c.moveToPosition(firstOther);
				
				final CharSequence sender = c.getString(SENDER_COLUMN);
				final CharSequence body = c.getString(BODY_COLUMN);
	
				n = new Notification(R.drawable.stat_notify_blip, sender + ": " + body, System.currentTimeMillis());
				n.ledARGB = getLedColor(context);
				n.flags = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_SHOW_LIGHTS;
				n.ledOffMS = 1000;
				n.ledOnMS = 200;
				n.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
				n.setLatestEventInfo(context, sender, body, 
						PendingIntent.getActivity(context, 0, new Intent(context, Dashboard.class), Intent.FLAG_ACTIVITY_NEW_TASK));
			}

			final int oldCount = sharedPreferences.getInt(MESSAGE_COUNT_KEY, 0);
			
			final int count = oldCount + c.getCount() - countDelta;

			if (count > 1 && n != null)
				n.number = count;

			sharedPreferences.edit()
				.putInt(LAST_SEEN_KEY, c.getInt(ID_COLUMN))
				.putInt(FREQUENCY_KEY, INITIAL_FREQUENCY_MS)
				.putInt(MESSAGE_COUNT_KEY, count)
			.commit();

			if (n != null) {
				NotificationManager nm = ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE));
				if (oldCount == 1) // we need a new one to get the number displayed
					nm.cancel(NOTIFICATION_ID);
				nm.notify(NOTIFICATION_ID, n);
			}
		}

		scheduleNextPoll(context);
	}

	private static void resetPollingFrequency(SharedPreferences sharedPreferences) {
		sharedPreferences.edit().remove(FREQUENCY_KEY).commit();
	}

	private static void scheduleNextPoll(Context context) {
		final int frequency = getAndBumpPollingFrequency(context);
		schedulePollAt(context, frequency);
	}

	private static void schedulePollAt(Context context, int delay) {
		Log.d("Bliper/MessagePoller", "scheduling next poll at " + delay);
		getAlarmManager(context).set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				SystemClock.elapsedRealtime() + delay, makeMessagePollerIntent(context));
	}

	private static PendingIntent makeMessagePollerIntent(Context context) {
		return PendingIntent.getBroadcast(context, 0, new Intent(context, MessagePoller.class), 0);
	}

	private static AlarmManager getAlarmManager(Context context) {
		return (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	}

	static private int getAndBumpPollingFrequency(Context context) {
		SharedPreferences sharedPreferences = getSharedPreferences(context);
		final int frequency = sharedPreferences.getInt(FREQUENCY_KEY, INITIAL_FREQUENCY_MS);
		final int new_frequency = frequency + FREQUENCY_DELTA_MS;
		if (new_frequency <= MAX_FREQUENCY_MS)
			sharedPreferences.edit().putInt(FREQUENCY_KEY, new_frequency).commit();
		return frequency;
	}

	private static int getLedColor(Context context) {
		if (IS_DREAM) // dream has seriously broken led drivers, use fake color for workaround
			return context.getResources().getColor(R.color.notification_led_htcdream);
		else
			return context.getResources().getColor(R.color.notification_led);
	}
	
	public static void resetPolling(Context context, Integer last_id) {
		SharedPreferences prefs = getSharedPreferences(context);
		if (prefs == null)
			return;
		Editor editor = prefs.edit();
		if (editor == null)
			return;
		editor
			.clear()
			.putInt(LAST_SEEN_KEY, last_id)
		.commit();
	}

	public int getLastSeen() {
		return sharedPreferences.getInt(LAST_SEEN_KEY, 0);
	}

	private static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences("blip.pl.polling", Context.MODE_PRIVATE);
	}

	public static void startPolling(Context context) {
		resetPollingFrequency(getSharedPreferences(context));
		scheduleNextPoll(context);
	}

	public static void stopPolling(Context context) {
		getAlarmManager(context).cancel(makeMessagePollerIntent(context));
	}
}
